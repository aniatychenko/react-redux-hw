import { SET_USER_ID, SET_USER_NAME, GET_MESSAGES, CHANGE_TEXT, EDIT_MESSAGE, ADD_MESSAGE, DELETE_MESSAGE, LIKE_MESSAGE } from "../actionTypes/messages";
import service from '../service';


export const getMessages = (messages) => ({
    type: GET_MESSAGES,
    payload: {
        messages
    }
})

export const setUserId = (userId) => ({
    type: SET_USER_ID,
    payload: {
        userId
    }
})
export const setUserName = (user) => ({
    type: SET_USER_NAME,
    payload: {
        user
    }
})

export const changeText = (text) => ({
    type: CHANGE_TEXT,
    payload: {
        text
    }
})


export const addMessage = (text, userId, userName) => ({
    type: ADD_MESSAGE,
    payload: {
        id: service.getNewMessageId(),
        text,
        userId,
        user: userName,
        createdAt: new Date()
    }
});

export const editMessage = (id, text) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        text
    }
});


export const likeMessage = (id, isLiked) => ({
    type: LIKE_MESSAGE,
    payload: {
        id,
        isLiked
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});