import { SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID, SHOW_MODAL, HIDE_MODAL } from "../actionTypes/modal";

export const setCurrentMessageId = messageId => ({
    type: SET_CURRENT_MESSAGE_ID,
    payload: {
        messageId
    }
});

export const dropCurrentMessageId = () => ({
    type: DROP_CURRENT_MESSAGE_ID
});

export const showModal = () => ({
    type: SHOW_MODAL
});

export const hideModal = () => ({
    type: HIDE_MODAL
});
