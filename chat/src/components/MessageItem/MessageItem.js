import React from 'react'
import styles from './MessageItem.module.scss'
import like from '../../images/like.svg'
import edit from '../../images/edit.svg'
import deleteIcon from '../../images/delete.svg'

export default class MessageItem extends React.Component {
    render() {
        if (this.props.currUser === this.props.userId) {
            return (
                <div className={styles['your-message-item-wrp']}>
                    <div className={styles['icons-wrp']}>
                        <img src={edit}
                            onClick={() => { this.props.editIcon(this.props.messageId) }}
                            alt="edit"
                            className={styles['edit-icon']} />
                        <img src={deleteIcon}
                            onClick={() => { this.props.delete(this.props.messageId) }}
                            alt="delete"
                            className={styles['delete-icon']} />
                    </div>
                    <div className={styles['your-message-item']} you='true'>
                        <div className={styles['message-time']}>{this.props.time}</div>
                        <div className={styles['message-text']}>
                            <span className={styles['user-name']}>{this.props.name}</span>
                            <p className={styles['text']}>{this.props.text}</p>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className={styles['message-item-wrp']}>
                <div className={styles['message-item']}>
                    <img src={this.props.avatar} alt="ava" className={styles['message-image']} />
                    <div className={styles['message-text']}>
                        <span className={styles['user-name']}>{this.props.name}</span>
                        <p className={styles['text']}>{this.props.text}</p>
                    </div>
                    <div className={styles['message-time']}>{this.props.time}</div>
                </div>
                <img src={like}
                    alt="like"
                    onClick={() => { this.props.like(this.props.messageId, this.props.isLiked) }}
                    className={this.props.isLiked ? styles['like-icon-liked'] : styles['like-icon']} />
            </div>
        )
    }
}