import React from 'react'
import styles from './EditMessage.module.scss'
import * as actions from '../../actions/messageModalActions';
import { editMessage } from '../../actions/messagesActions'
import { connect } from 'react-redux'

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.messageId !== this.props.messageId) {
            const message = this.props.messages.find(message => message.id === nextProps.messageId);
            this.setState({ ...this.state, text: message ? message.text : '' });
        }
    }

    onCancel() {
        this.props.dropCurrentMessageId()
        this.props.hideModal();
    }
    onEdit() {
        this.props.hideModal();
        this.props.editMessage(this.props.messageId, this.state.text);
        this.props.dropCurrentMessageId()

    }
    onChangeText(text) {
        this.setState(
            {
                ...this.state,
                text: text
            })
    }
    getModal() {
        return (
            <div className={styles['modal-background']}>
                <div className={styles['modal-block']}>
                    <textarea
                        onChange={(ev) => this.onChangeText(ev.target.value)}
                        className={styles['edit-textarea']}
                        value={this.state.text}>
                    </textarea>
                    <div className={styles['btn-wrp']}>
                        <button
                            className={styles['cancel-button']} onClick={this.onCancel}>Cancel</button>
                        <button
                            className={styles['edit-button']} onClick={this.onEdit}>Edit</button>
                    </div>
                </div>
            </div>
        )
    }
    render() {
        const isShown = this.props.isShown;
        return isShown ? this.getModal() : null
    }
}
const mapStateToProps = (state) => {
    return {
        messages: state.messages.messages,
        isShown: state.messageModal.isShown,
        messageId: state.messageModal.messageId
    }
};

const mapDispatchToProps = {
    ...actions,
    editMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);