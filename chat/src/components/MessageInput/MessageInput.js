import React from 'react'
import styles from './MessageInput.module.scss'
import sendIcon from '../../images/arrow-circle-up-solid.svg'

export default class MessageInput extends React.Component {
    render() {
        return (
            <div className={styles['message-input']}>
                <textarea
                    placeholder="Message"
                    type="text"
                    onChange={(ev) => this.props.onChangeText(ev.target.value)}
                    className={styles['message-send-text']}
                    value={this.props.text || ''}>
                </textarea>
                <button
                    onClick={(ev) => this.props.send(this.props.currentUserId, this.props.currentUserName)}
                    id={styles['send-button']}>
                    <img src={sendIcon} alt="send" width="32px" />Send</button>
            </div>
        )
    }
}