import React from 'react'
import styles from './Chat.module.scss'
import Header from '../Header/Header'
import MessageList from '../MessageList/MessageList'
import MessageInput from '../MessageInput/MessageInput'
import * as actions from '../../actions/messagesActions'
import { setCurrentMessageId, showModal, dropCurrentMessageId } from '../../actions/messageModalActions';
import { connect } from 'react-redux'
import Loading from '../Loading/Loading'

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.onSend = this.onSend.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onEditIcon = this.onEditIcon.bind(this);
        this.onLike = this.onLike.bind(this);
        this.onKeyUpEv = this.onKeyUpEv.bind(this);

    }
    participants = new Set();
    participantsId = new Set();
    componentDidMount() {
        document.addEventListener("keyup", this.onKeyUpEv);
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then(result => result.json())
            .then(data => {
                data.sort(function (a, b) {
                    return new Date(a.createdAt) - new Date(b.createdAt);
                });
                data.forEach(message => {
                    message.isLiked = false;
                    this.participantsId.add(message.userId);
                    this.participants.add(message.user);
                })
                this.props.getMessages(data)
                this.props.setUserId(Array.from(this.participantsId)[Math.floor(Math.random() * (this.participantsId.size - 0))],)
                this.props.setUserName(data.find(user => user.userId === this.props.currentUserId).user)
            })
    }
    componentWillUnmount() {
        document.removeEventListener("keyup", this.onKeyUpEv);
    }
    onLike = (id, isLiked) => {
        this.props.likeMessage(id, isLiked)
    }
    onEditIcon = (messageId) => {
        this.props.setCurrentMessageId(messageId);
        this.props.showModal();
    }
    onDelete = (id) => {
        this.props.deleteMessage(id)
    }
    onChangeText = (text) => {
        this.props.changeText(text)
    }
    onSend = (userId, user) => {
        if (this.props.messageId) {
            this.props.editMessage(this.props.messageId, this.props.text);
        }
        else {
            this.props.addMessage(this.props.text, userId, user);
        }
        this.onChangeText('');
        this.props.dropCurrentMessageId();
    }
    onKeyUpEv = (code) => {
        if (code.keyCode === 38) {
            const currentUserMessagesArray = this.props.messages.filter(message => message.userId === this.props.currentUserId);
            const lastMessage = currentUserMessagesArray[currentUserMessagesArray.length - 1]
            this.props.setCurrentMessageId(lastMessage.id);
            this.onChangeText(lastMessage.text);
        }
    }

    render() {
        const messagesAmount = this.props.messages.length;
        const lastMessageTime = messagesAmount ? new Date(this.props.messages[messagesAmount - 1].createdAt) : null;
        if (this.props.showLoader) {
            return <Loading />;
        }
        return (
            <div className={styles['chat-wrp']}>
                <Header
                    participants={this.participants.size}
                    messagesAmount={messagesAmount}
                    lastMessage={lastMessageTime.getHours() + ":" + lastMessageTime.getMinutes()}
                />
                <MessageList
                    messageItemsInfo={this.props.messages}
                    currUser={this.props.currentUserId}
                    like={this.onLike}
                    delete={this.onDelete}
                    editIcon={this.onEditIcon}
                />
                <MessageInput
                    currentUserId={this.props.currentUserId}
                    currentUserName={this.props.currentUserName}
                    text={this.props.text}
                    onChangeText={this.onChangeText}
                    send={this.onSend} />
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        ...state.messages,
        ...state.messageModal
    }
};

const mapDispatchToProps = {
    ...actions,
    setCurrentMessageId,
    dropCurrentMessageId,
    showModal
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);