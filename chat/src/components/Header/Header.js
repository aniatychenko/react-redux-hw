import React from 'react'
import styles from './Header.module.scss'

export default class Header extends React.Component {
    render() {
        const { participants, messagesAmount, lastMessage } = this.props;
        return (
            <div className={styles['chat-header']}>
                <p>My Chat</p>
                <p>{participants} members</p>
                <p>{messagesAmount} messages</p>
                <p>Last message at: {lastMessage}</p>
            </div>
        )
    }
}