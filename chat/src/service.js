const getNewMessageId = () => (new Date()).getTime();

// eslint-disable-next-line
export default {
    getNewMessageId
};