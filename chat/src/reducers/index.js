import {combineReducers} from 'redux'
import messages from './messages'
import messageModal from './messageModal'

const rootReducer = combineReducers({
    messages,
    messageModal
})

export default rootReducer