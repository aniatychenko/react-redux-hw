import { SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID, SHOW_MODAL, HIDE_MODAL } from "../actionTypes/modal";


const initialState = {
    messageId: '',
    isShown: false
};
// eslint-disable-next-line
export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_MESSAGE_ID: {
            const { messageId } = action.payload;
            return {
                ...state,
                messageId: messageId
            };
        }
        case DROP_CURRENT_MESSAGE_ID: {
            return {
                ...state,
                messageId: ''
            };
        }

        case SHOW_MODAL: {
            return {
                ...state,
                isShown: true
            };
        }

        case HIDE_MODAL: {
            return {
                ...state,
                isShown: false
            };
        }

        default:
            return state;
    }
}