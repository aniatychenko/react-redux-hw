import { SET_USER_ID, SET_USER_NAME, GET_MESSAGES, ADD_MESSAGE, CHANGE_TEXT, EDIT_MESSAGE, LIKE_MESSAGE, DELETE_MESSAGE } from "../actionTypes/messages";
const initialState = {
    messages: [],
    currentUserId: '',
    currentUserName: '',
    showLoader: true,
}

// eslint-disable-next-line
export default function (state = initialState, action) {
    switch (action.type) {

        case SET_USER_ID: {
            const { userId } = action.payload
            return {
                ...state,
                currentUserId: userId
            };
        }

        case SET_USER_NAME: {
            const { user } = action.payload
            return {
                ...state,
                currentUserName: user
            };
        }

        case GET_MESSAGES: {
            const { messages } = action.payload
            return {
                ...state,
                messages: messages,
                showLoader: false
            };
        }

        case CHANGE_TEXT: {
            const { text } = action.payload
            return {
                ...state,
                text: text
            };
        }

        case ADD_MESSAGE: {
            const { id, text, userId, user, createdAt } = action.payload;
            const newMessage = { id, text, userId, user, createdAt };
            return {
                ...state,
                messages: [...state.messages, newMessage]
            };
        }

        case EDIT_MESSAGE: {
            const { id, text } = action.payload;
            const editMessages = state.messages.map(message => {
                if (message.id === id) {
                    return {
                        ...message,
                        text
                    };
                } else {
                    return message;
                }
            });

            return {
                ...state,
                messages: editMessages
            };
        }

        case LIKE_MESSAGE: {
            const { id, isLiked } = action.payload;
            const editMessages = state.messages.map(message => {
                if (message.id === id) {
                    return {
                        ...message,
                        isLiked: !isLiked
                    };
                } else {
                    return message;
                }
            });

            return {
                ...state,
                messages: editMessages
            };
        }

        case DELETE_MESSAGE: {
            const { id } = action.payload;
            const filteredMessages = state.messages.filter(message => message.id !== id);
            return {
                ...state,
                messages: filteredMessages
            };
        }

        default:
            return state;
    }
}
